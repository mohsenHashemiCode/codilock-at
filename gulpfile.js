let gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    browserSync = require("browser-sync").create(),
    nunjucksRender = require('gulp-nunjucks-render'),
    rtlcss = require('gulp-rtlcss'),
    rename = require('gulp-rename');

function style() {
    return gulp.src('app/assets/scss/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on("error", sass.logError)
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(rtlcss()) // Convert to RTL.
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('app/assets/css'))
        .pipe(browserSync.stream())
}

function rtlStyle() {
    //
}

function html() {
    return gulp.src('app/pages/**/*.+(html|nunjucks)')
        // Renders template with nunjucks
        .pipe(nunjucksRender({
            path: ['app/templates']
        }))
        // output files in app folder
        .pipe(gulp.dest('app'));
}

function reload() {
    browserSync.reload();
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });
    gulp.watch('app/assets/scss/**/*.scss', style);
    gulp.watch('app/pages/**/*.+(html|nunjucks)', html);
    gulp.watch('app/pages/**/*.+(html|nunjucks)', reload);
}

exports.watch = watch