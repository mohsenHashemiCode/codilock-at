let sidebar_toggle_btn = document.getElementById('sidebar-toggle');
let sidebar = document.getElementById('sidebar');
let page_content = document.getElementById('page-content')
sidebar_toggle_btn.addEventListener('click', function() {
    this.classList.toggle('active');
    sidebar.classList.toggle('active');
    sidebar.classList.toggle('shrinked');
    page_content.classList.toggle('shrinked');

    if(sidebar.classList.contains('active')){
        console.log(document.querySelector('.navbar-brand .brand-sm'))
        document.querySelector('.navbar-brand .brand-sm').classList.add('d-block');
        document.querySelector('.navbar-brand .brand-big').classList.remove('d-block');
    } else {
        document.querySelector('.navbar-brand .brand-sm').classList.remove('d-block');
        document.querySelector('.navbar-brand .brand-big').classList.add('d-block');
    }
});

// ------------------------------------------------------- //
// Search Popup
// ------------------------------------------------------ //
document.querySelector('.search-open').addEventListener('click', function(){
    document.querySelector('.search-panel').style.display = 'flex';
});
document.querySelector('.search-panel .close-btn').addEventListener('click', function(){
    document.querySelector('.search-panel').style.display = 'none';
});